mod lecture_fichier;

#[derive(Debug)]
pub struct Robot{
    id: String,
    x: u32,
    y: u32,
    orientation: Orientation,
}

#[derive(Debug)]
enum Orientation {
    N,
    E,
    S,
    W,
}

enum Instructions {
    L {x: i32, y: i32},
    R {x: i32, y: i32},
    F {x: i32, y: i32},
    N {x: i32, y: i32},
}

pub struct Terrain {
    x_max: u32,
    y_max: u32,
}

/* La fonction collision renvoie en sortie standart que le robot_x, celui qui vient de se déplacer,
** est entré en collision avec un autre robot.
*/
fn collision(robot_x: Robot, robot_y: Robot){
    if (robot_x.x == robot_y.x) && (robot_x.y == robot_y.y){
        println!("Robot ID<{}> Collision en ({},{})",robot_x.id,robot_x.x,robot_x.y)
    }
}

/* La fonction deplacement prend en arguments le robot à deplacer et une instruction.
** Elle renvoi une structure Robot avec les modifications effectuées.
*/
fn deplacement(mut robot: Robot, instruction: &str) -> Robot {
    match instruction {
        "L" => match robot.orientation {
            Orientation::N => robot.orientation = Orientation::W,
            Orientation::S => robot.orientation = Orientation::E,
            Orientation::E => robot.orientation = Orientation::N,
            Orientation::W => robot.orientation = Orientation::S,
        }
        "R" => match robot.orientation {
            Orientation::N => robot.orientation = Orientation::E,
            Orientation::S => robot.orientation = Orientation::W,
            Orientation::E => robot.orientation = Orientation::S,
            Orientation::W => robot.orientation = Orientation::N,
        }
        "F" => match robot.orientation {
            Orientation::N => robot.y += 1,
            Orientation::S => robot.y -= 1,
            Orientation::E => robot.x += 1,
            Orientation::W => robot.x -= 1,
        }
        _ => (),
    }
    robot
}

fn main() {
    // création du monde :
    let mut line_of_text: Vec<String> = Vec::new();
    let data_of_world: Vec<u32>;

    line_of_text = lecture_fichier::linebyline(line_of_text);
    lecture_fichier::del_spaces(&mut line_of_text[0]);
    data_of_world = lecture_fichier::for_the_world(&mut line_of_text[0]);
    
    let terrain = Terrain {
        x_max: data_of_world[0],
        y_max: data_of_world[1],
    };
    println!("Terrain: x:{}, y:{}\n", terrain.x_max, terrain.y_max);

    // Création du robot 1
    let mut robot_1: Robot;
    lecture_fichier::del_spaces(&mut line_of_text[1]);
    robot_1 = lecture_fichier::for_the_robots(&mut line_of_text[1]);
    robot_1.id = "Vincent".to_string();

    // Affichage initial du robot 1 et de ses attributs
    println!("robot_1 :\nid : {:?}\nx : {:?}\ny : {:?}\norientation : {:?}", robot_1.id, robot_1.x, robot_1.y, robot_1.orientation);

    // Recuperation et affichage de la chaine de deplacement du robot ci-dessus
    let deplacement_robot_1: Vec<String>;
    deplacement_robot_1 = lecture_fichier::vec_data_of_line(&mut line_of_text[2]);
    println!("{:?}\n", deplacement_robot_1);

    // Création du robot 2
    let mut robot_2: Robot;
    lecture_fichier::del_spaces(&mut line_of_text[3]);
    robot_2 = lecture_fichier::for_the_robots(&mut line_of_text[3]);
    robot_2.id = "Ludivine".to_string();

    // Affichage initial du robot 2 et de ses attributs
    println!("robot_2 :\nid : {:?}\nx : {:?}\ny : {:?}\norientation : {:?}", robot_2.id, robot_2.x, robot_2.y, robot_2.orientation);

    // Recuperation et affichage de la chaine de deplacement du robot ci-dessus
    let deplacement_robot_2: Vec<String>;
    deplacement_robot_2 = lecture_fichier::vec_data_of_line(&mut line_of_text[4]);
    println!("{:?}\n", deplacement_robot_2);

    loop {
        let mut i = 0;
        for _elements in &deplacement_robot_1 {
            println!("{:?}", &deplacement_robot_1[i]);
            robot_1 = deplacement(robot_1, &deplacement_robot_1[i]);
            println!("robot_1 :\nid : {:?}\nx : {:?}\ny : {:?}\norientation : {:?}\n", robot_1.id, robot_1.x, robot_1.y, robot_1.orientation);
            i += 1;
        }
        i = 0;
        for _elements in &deplacement_robot_2 {
            println!("{:?}", &deplacement_robot_2[i]);
            robot_2 = deplacement(robot_2, &deplacement_robot_2[i]);
            println!("robot_2 :\nid : {:?}\nx : {:?}\ny : {:?}\norientation : {:?}\n", robot_2.id, robot_2.x, robot_2.y, robot_2.orientation);
            i += 1;
        }
        break;
    }
}
