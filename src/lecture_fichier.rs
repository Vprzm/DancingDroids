use std::io::{BufReader,BufRead};
use std::fs::File;
use crate::*;

/* La fonction linebyline permet d'ajouter dans un vecteur chaque ligne d'un fichier texte (les lignes
** vide ne sont pas pris en comtpe). Elle prend en paramètre un vecteur de type String qui contiendra 
** les lignes du texte et elle le renvoie.
*/
pub fn linebyline(mut texte: Vec<String>)-> Vec<String>{
    let space: String = "".to_string();
    let file = File::open("../two_robots.txt").unwrap(); //
    for line in BufReader::new(file).lines() {
        let ligne = line.unwrap();
        if ligne == space{
            () } else{ texte.push(ligne);}
 }
    return texte
}

/* La fonction del_spaces permet de supprimer les espaces d'une chaîne de caractère. Elle prend en
** paramètre une chaîne de caractère et la renvoie.
*/
pub fn del_spaces(s: &mut String) -> &mut String{
  s.retain(|c| !c.is_whitespace());
  return s
}

/* La fonction vec_data_of_line permet de mettre chaque caractère  (que l'on transforme en type
** String) qui constitue une donnée de la chaîne dans un vecteur.
*/
pub fn vec_data_of_line(data: &mut String) -> Vec<String>{
    let mut vec_data: Vec<String>= Vec::new();
    for c in data.chars(){
        vec_data.push(c.to_string());
    }
    return vec_data;
}

/* La fonction convert_string_in_int permet de transformer une chaîne de caractère en type entier */
pub fn convert_string_in_int(data: String) -> u32{
    let integer: u32 = data.parse::<u32>().unwrap();
    return integer;
}

/* Fonction réservé à l'initialisation du monde: elle créait un vecteur contenant les données du monde
** en type entier
*/
pub fn for_the_world(data: &mut String) -> Vec<u32>{
    let mut vec_data: Vec<String>= Vec::new();
    let mut integer: Vec<u32>= Vec::new();
    for c in data.chars(){
        vec_data.push(c.to_string());
    }
    for s in vec_data {
        integer.push(s.parse::<u32>().unwrap());
    }
    return integer;
}

/* Fonction qui initialise un robot */
pub fn for_the_robots(data: &mut String) -> Robot {
    let mut vec_data: Vec<String>= Vec::new();
    let mut integer: Vec<u32>= Vec::new();
    let mut robots = Robot {
        id: String::new(),
        x: 0,
        y: 0,
        orientation: Orientation::N,
    };
    for c in data.chars(){
        vec_data.push(c.to_string());
    }
    for s in vec_data {
        if s == "N".to_string() {
            robots.orientation = Orientation::N;
            break;
        }
        if s == "S".to_string() {
            robots.orientation = Orientation::S;
            break;
        }
        if s == "E".to_string() {
            robots.orientation = Orientation::E;
            break;
        }
        if s == "W".to_string() {
            robots.orientation = Orientation::W;
            break;
        }
        integer.push(s.parse::<u32>().unwrap());
    }
    robots.x = integer[0];
    robots.y = integer[1];
    return robots;
}